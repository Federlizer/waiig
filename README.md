# Writing An Interpreter In Go (waiig)

This repository contains the source code for the Monkey language interpreter. The code follows the ["Writing An
Interpreter In Go"](https://interpreterbook.com/) book by Thorsen Ball.

### Missing features from Monkey language

- float numbers
- hex, octa, binary literals
